package com.example.soalujianardian.service;

import com.example.soalujianardian.dto.KaryawanDto;
import com.example.soalujianardian.model.Karyawan;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface KaryawanService {

    Page<Karyawan> getAllWarga(String search, Long page);
    Karyawan getWarga(Long id);
    Karyawan addWarga(KaryawanDto warga);
    Karyawan editWarga(Long id, Karyawan warga);
    Map<String ,Boolean> deleteWargaById(Long id);
}
