package com.example.soalujianardian.service;

import com.example.soalujianardian.dto.DiagnosaDto;
import com.example.soalujianardian.model.Diagnosa;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface DiagnosaService {
    Page<Diagnosa> getAllDiagnosa(String search, Long page);
    Diagnosa getDiagnosa(Long id);
    Diagnosa addDiagnosa(DiagnosaDto diagnosa);
    Diagnosa editDiagnosa(Long id,Diagnosa diagnosa);
    Map<String ,Boolean> deleteDiagnosaById(Long id);
}
