package com.example.soalujianardian.service;

import com.example.soalujianardian.dto.GuruDto;
import com.example.soalujianardian.model.Guru;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface GuruService {
    Page<Guru> getAllGuru(String search, Long page);
    Guru getGuru(Long id);
    Guru addGuru(GuruDto guru);
    Guru editGuru(Long id,Guru guru);
    Map<String ,Boolean> deleteGuruById(Long id);

}
