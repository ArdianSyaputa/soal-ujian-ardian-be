package com.example.soalujianardian.service;

import com.example.soalujianardian.model.DaftarObat;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface DaftarObatService {
    Page<DaftarObat> getAllDaftarObat(String search, Long page);
    DaftarObat getDaftarObat(Long id);
    DaftarObat addDaftarObat(DaftarObat daftarObat);
    DaftarObat editDaftarObat(Long id,DaftarObat daftarObat);
    Map<String ,Boolean> deleteDaftarObatById(Long id);
}
