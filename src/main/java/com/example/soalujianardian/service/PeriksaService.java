package com.example.soalujianardian.service;

import com.example.soalujianardian.dto.PeriksaDto;
import com.example.soalujianardian.model.Periksa;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface PeriksaService {
    Page<Periksa> getAllWarga(String search, Long page);
    Periksa getWarga(Long id);
    Periksa addWarga(PeriksaDto periksa);
    Periksa editWarga(Long id, Periksa periksa);
    Map<String ,Boolean> deleteWargaById(Long id);
}
