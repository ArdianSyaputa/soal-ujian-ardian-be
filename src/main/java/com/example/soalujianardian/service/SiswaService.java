package com.example.soalujianardian.service;

import com.example.soalujianardian.dto.SiswaDto;
import com.example.soalujianardian.model.Siswa;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface SiswaService {
    Page<Siswa> getAllSiswa(String search, Long page);
    Siswa getSiswa(Long id);
    Siswa addSiswa(SiswaDto siswa);
    Siswa editSiswa(Long id,Siswa siswa);
    Map<String ,Boolean> deleteSiswaById(Long id);
}
