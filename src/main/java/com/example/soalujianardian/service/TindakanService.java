package com.example.soalujianardian.service;

import com.example.soalujianardian.dto.TindakanDto;
import com.example.soalujianardian.model.Tindakan;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface TindakanService {

    Page<Tindakan> getAllTindakan(String search, Long page);
    Tindakan getTindakan(Long id);
    Tindakan addTindakan(TindakanDto tindakan);
    Tindakan editTindakan(Long id,Tindakan tindakan);
    Map<String ,Boolean> deleteTindakanById(Long id);
}
