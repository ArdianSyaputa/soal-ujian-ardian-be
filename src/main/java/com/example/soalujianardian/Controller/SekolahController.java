package com.example.soalujianardian.Controller;


import com.example.soalujianardian.Login.Service.RegisterService;
import com.example.soalujianardian.Login.model.Login;
import com.example.soalujianardian.Login.model.Password;
import com.example.soalujianardian.Login.model.Register;
import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import org.hibernate.sql.Update;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("/sekolah")
@CrossOrigin(origins = "http://localhost:3000")
public class SekolahController {
    @Autowired
    private RegisterService sekolahService;

    @Autowired
   private ModelMapper modelMapper;

    @PostMapping("/register")
    public CommonResponse<Register> registrasi(@RequestBody Register sekolah) {
        return ResponseHelper.ok(sekolahService.registrasi(sekolah));
    }

    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponseHelper.ok(sekolahService.login(login));
    }


    @GetMapping("/{id}")
    public CommonResponse<Register> getById(@PathVariable Long id) {
        return ResponseHelper.ok(sekolahService.getById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<Register> update(@PathVariable("id") Long id,@RequestBody Update update) {
        return ResponseHelper.ok(sekolahService.update(id, modelMapper.map(update , Register.class)));
    }
    @PutMapping(path = "/password/{id}")
    public CommonResponse<Register> updatePassword(@PathVariable("id") Long id,@RequestBody Password password) {
        return ResponseHelper.ok(sekolahService.update(id, modelMapper.map(password , Register.class)));
    }
//    @PutMapping(path = "/foto/{id}", consumes = "multipart/form-data")
//    public CommonResponse<Sekolah> updateFoto(@PathVariable("id") Long id, Foto foto, @RequestPart("file") MultipartFile multipartFile) {
//        return ResponseHelper.ok(sekolahService.updateFoto(id, modelMapper.map(foto , Sekolah.class) , multipartFile));
//    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteSekolah(@PathVariable("id") Long id) {
        return ResponseHelper.ok(sekolahService.deleteSekolah(id));
    }
//    @GetMapping
//    public CommonResponse<Page<Register>> getAll(@RequestParam(name = "query", required = false) String query, Long page) {
//        return ResponseHelper.ok(sekolahService.getAll(query == null ? "" : query, page));
//    }
}
