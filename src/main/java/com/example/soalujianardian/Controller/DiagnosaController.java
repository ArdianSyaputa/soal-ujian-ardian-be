package com.example.soalujianardian.Controller;

import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import com.example.soalujianardian.dto.DiagnosaDto;
import com.example.soalujianardian.model.Diagnosa;
import com.example.soalujianardian.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/diagnosa")
public class DiagnosaController {

    @Autowired
    DiagnosaService diagnosaService;

    @GetMapping
    public CommonResponse<Page<Diagnosa>> getAllDiagnosa(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(diagnosaService.getAllDiagnosa(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteDiagnosaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( diagnosaService.deleteDiagnosaById(id));
    }

    @GetMapping("/{id}")
    public CommonResponse <Diagnosa> getDiagnosa(@PathVariable("id")Long id){
        return ResponseHelper.ok( diagnosaService.getDiagnosa(id));
    }
    @PostMapping
    public CommonResponse<Diagnosa> addDiagnosa(@RequestBody DiagnosaDto siswa ){
        return ResponseHelper.ok(diagnosaService.addDiagnosa(siswa));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse <Diagnosa> editTodoListById(@PathVariable("id") Long id,@RequestBody Diagnosa siswa) {
        return ResponseHelper.ok( diagnosaService.editDiagnosa(id, siswa));
    }
}
