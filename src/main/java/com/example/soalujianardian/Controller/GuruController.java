package com.example.soalujianardian.Controller;

import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import com.example.soalujianardian.dto.GuruDto;
import com.example.soalujianardian.model.Guru;
import com.example.soalujianardian.service.GuruService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/guru")
public class GuruController {

    @Autowired
    GuruService guruService;

    @GetMapping
    public CommonResponse<Page<Guru>> getAllGuru(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(guruService.getAllGuru(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteGuruById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( guruService.deleteGuruById(id));
    }

    @GetMapping("/{id}")
    public CommonResponse <Guru> getGuru(@PathVariable("id")Long id){
        return ResponseHelper.ok( guruService.getGuru(id));
    }
    @PostMapping
    public CommonResponse<Guru> addGuru(GuruDto siswa ){
        return ResponseHelper.ok(guruService.addGuru(siswa));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse <Guru> editTodoListById(@PathVariable("id") Long id,@RequestBody Guru siswa) {
        return ResponseHelper.ok( guruService.editGuru(id, siswa));
    }

}
