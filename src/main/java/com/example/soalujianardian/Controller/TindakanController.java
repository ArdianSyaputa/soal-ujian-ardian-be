package com.example.soalujianardian.Controller;

import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import com.example.soalujianardian.dto.TindakanDto;
import com.example.soalujianardian.model.Tindakan;
import com.example.soalujianardian.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tindakan")
public class TindakanController {

    @Autowired
    TindakanService tindakanService;

    @GetMapping
    public CommonResponse<Page<Tindakan>> getAllTindakan(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(tindakanService.getAllTindakan(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteTindakanById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( tindakanService.deleteTindakanById(id));
    }

    @GetMapping("/{id}")
    public CommonResponse<Tindakan> getTindakan(@PathVariable("id")Long id){
        return ResponseHelper.ok( tindakanService.getTindakan(id));
    }
    @PostMapping
    public CommonResponse<Tindakan> addTindakan(TindakanDto siswa ){
        return ResponseHelper.ok(tindakanService.addTindakan(siswa));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse <Tindakan> editTodoListById(@PathVariable("id") Long id,@RequestBody Tindakan siswa) {
        return ResponseHelper.ok( tindakanService.editTindakan(id, siswa));
    }
}
