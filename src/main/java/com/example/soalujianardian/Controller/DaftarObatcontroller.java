package com.example.soalujianardian.Controller;

import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import com.example.soalujianardian.model.DaftarObat;
import com.example.soalujianardian.service.DaftarObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/obat")
public class DaftarObatcontroller {

    @Autowired
    DaftarObatService daftarObatService;

    @GetMapping
    public CommonResponse<Page<DaftarObat>> getAllDaftarObat(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(daftarObatService.getAllDaftarObat(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteDaftarObatById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( daftarObatService.deleteDaftarObatById(id));
    }

    @GetMapping("/{id}")
    public CommonResponse <DaftarObat> getDaftarObat(@PathVariable("id")Long id){
        return ResponseHelper.ok( daftarObatService.getDaftarObat(id));
    }
    @PostMapping
    public CommonResponse<DaftarObat> addDaftarObat(DaftarObat daftarObat ){
        return ResponseHelper.ok(daftarObatService.addDaftarObat(daftarObat));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse <DaftarObat> editTodoListById(@PathVariable("id") Long id,@RequestBody DaftarObat daftarObat) {
        return ResponseHelper.ok( daftarObatService.editDaftarObat(id, daftarObat));
    }
}
