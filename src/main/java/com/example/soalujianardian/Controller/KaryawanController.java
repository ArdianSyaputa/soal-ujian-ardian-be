package com.example.soalujianardian.Controller;

import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import com.example.soalujianardian.dto.KaryawanDto;
import com.example.soalujianardian.model.Karyawan;
import com.example.soalujianardian.service.KaryawanService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/karyawan")
public class KaryawanController {

    @Autowired
    KaryawanService karyawanService;

    @GetMapping
    public CommonResponse<Page<Karyawan>> getAllWarga(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(karyawanService.getAllWarga(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteWargaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( karyawanService.deleteWargaById(id));
    }

    @GetMapping("/{id}")
    public CommonResponse <Karyawan> getWarga(@PathVariable("id")Long id){
        return ResponseHelper.ok( karyawanService.getWarga(id));
    }
    @PostMapping
    public CommonResponse<Karyawan> addWarga(KaryawanDto siswa ){
        return ResponseHelper.ok(karyawanService.addWarga(siswa));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse <Karyawan> editTodoListById(@PathVariable("id") Long id, @RequestBody Karyawan siswa) {
        return ResponseHelper.ok( karyawanService.editWarga(id, siswa));
    }
}
