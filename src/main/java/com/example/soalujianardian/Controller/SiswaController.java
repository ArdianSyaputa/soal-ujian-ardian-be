package com.example.soalujianardian.Controller;

import com.example.soalujianardian.Login.response.CommonResponse;
import com.example.soalujianardian.Login.response.ResponseHelper;
import com.example.soalujianardian.dto.SiswaDto;
import com.example.soalujianardian.model.Siswa;
import com.example.soalujianardian.service.SiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/siswa")
public class SiswaController {

    @Autowired
    SiswaService siswaService;

    @GetMapping
    public CommonResponse<Page<Siswa>> getAllSiswa(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(siswaService.getAllSiswa(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteSiswaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( siswaService.deleteSiswaById(id));
    }

    @GetMapping("/{id}")
    public CommonResponse <Siswa> getSiswa(@PathVariable("id")Long id){
        return ResponseHelper.ok( siswaService.getSiswa(id));
    }
    @PostMapping
    public CommonResponse<Siswa> addSiswa(SiswaDto siswa ){
        return ResponseHelper.ok(siswaService.addSiswa(siswa));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse <Siswa> editTodoListById(@PathVariable("id") Long id,@RequestBody Siswa siswa) {
        return ResponseHelper.ok( siswaService.editSiswa(id, siswa));
    }
}
