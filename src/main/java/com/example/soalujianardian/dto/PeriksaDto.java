package com.example.soalujianardian.dto;

public class PeriksaDto {

private Long namaSiswa;
private Long namaGuru;
private Long namaKaryawan;
private String keterangan;

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Long getNamaSiswa() {
        return namaSiswa;
    }

    public void setNamaSiswa(Long namaSiswa) {
        this.namaSiswa = namaSiswa;
    }

    public Long getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(Long namaGuru) {
        this.namaGuru = namaGuru;
    }

    public Long getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(Long namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }
}
