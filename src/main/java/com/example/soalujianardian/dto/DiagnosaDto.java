package com.example.soalujianardian.dto;

public class DiagnosaDto {
    private String nama_Diagnosa;

    public String getNama_Diagnosa() {
        return nama_Diagnosa;
    }

    public void setNama_Diagnosa(String nama_Diagnosa) {
        this.nama_Diagnosa = nama_Diagnosa;
    }
}
