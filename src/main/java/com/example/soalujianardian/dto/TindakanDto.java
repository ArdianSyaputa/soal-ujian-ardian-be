package com.example.soalujianardian.dto;

public class TindakanDto {
    private String nama_Tindakan;

    private Long namaSiswa;

    public Long getNamaSiswa() {
        return namaSiswa;
    }

    public void setNamaSiswa(Long namaSiswa) {
        this.namaSiswa = namaSiswa;
    }

    public String getNama_Tindakan() {
        return nama_Tindakan;
    }

    public void setNama_Tindakan(String nama_Tindakan) {
        this.nama_Tindakan = nama_Tindakan;
    }
}
