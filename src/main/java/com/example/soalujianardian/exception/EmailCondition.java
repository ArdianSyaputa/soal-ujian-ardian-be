package com.example.soalujianardian.exception;

public class EmailCondition extends RuntimeException {
    public EmailCondition(String message) {
        super(message);
    }
}
