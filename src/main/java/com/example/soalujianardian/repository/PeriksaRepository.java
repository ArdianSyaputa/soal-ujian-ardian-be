package com.example.soalujianardian.repository;

import com.example.soalujianardian.model.Periksa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriksaRepository extends JpaRepository<Periksa,Long> {
    @Query(value = "SELECT * FROM periksa  WHERE nama LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
    Page<Periksa> searchfindAll(String search, Pageable pageable);
}
