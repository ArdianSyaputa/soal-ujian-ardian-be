package com.example.soalujianardian.repository;

import com.example.soalujianardian.model.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan,Long> {
    @Query(value = "SELECT * FROM karyawan  WHERE namaKaryawan LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
    Page<Karyawan> searchfindAll(String search, Pageable pageable);
}
