package com.example.soalujianardian.repository;

import com.example.soalujianardian.model.Siswa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SiswaRepository extends JpaRepository<Siswa,Long> {
    @Query(value = "SELECT * FROM siswa  WHERE nama LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
    Page<Siswa> searchfindAll(String search, Pageable pageable);
}
