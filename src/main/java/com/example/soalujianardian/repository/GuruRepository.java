package com.example.soalujianardian.repository;

import com.example.soalujianardian.model.Guru;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GuruRepository extends JpaRepository<Guru,Long> {
    @Query(value = "SELECT * FROM guru  WHERE nama_guru LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
    Page<Guru> searchfindAll(String search, Pageable pageable);
}
