package com.example.soalujianardian.repository;

import com.example.soalujianardian.model.Tindakan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TindakanRepository extends JpaRepository<Tindakan,Long> {
    @Query(value = "SELECT * FROM tindakan  WHERE nama_Tindakan LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
    Page<Tindakan> searchfindAll(String search, Pageable pageable);
}
