package com.example.soalujianardian.Impl;

import com.example.soalujianardian.dto.TindakanDto;
import com.example.soalujianardian.exception.NotFoundException;
import com.example.soalujianardian.model.Tindakan;
import com.example.soalujianardian.repository.TindakanRepository;
import com.example.soalujianardian.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TindakanImpl implements TindakanService {

    @Autowired
    TindakanRepository tindakanRepository;

    @Override
    public Page<Tindakan> getAllTindakan(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return tindakanRepository.searchfindAll(search,pageable);
    }

    @Override
    public Tindakan getTindakan(Long id) {
        var tindakan = tindakanRepository.findById(id).get();
        return tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Tindakan addTindakan(TindakanDto tindakan) {
        Tindakan kerja = new Tindakan();
        kerja.setNama_Tindakan(tindakan.getNama_Tindakan());
        return tindakanRepository.save(kerja);
    }

    @Override
    public Tindakan editTindakan(Long id, Tindakan tindakan) {
        Tindakan kerja = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        kerja.setNama_Tindakan(tindakan.getNama_Tindakan());
        return tindakanRepository.save(kerja);
    }

    @Override
    public Map<String, Boolean> deleteTindakanById(Long id) {
        try {
            tindakanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
