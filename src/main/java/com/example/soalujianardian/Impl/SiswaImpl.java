package com.example.soalujianardian.Impl;

import com.example.soalujianardian.dto.SiswaDto;
import com.example.soalujianardian.exception.NotFoundException;
import com.example.soalujianardian.model.Siswa;
import com.example.soalujianardian.repository.SiswaRepository;
import com.example.soalujianardian.service.SiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SiswaImpl implements SiswaService {

@Autowired
SiswaRepository siswaRepository;

    @Override
    public Page<Siswa> getAllSiswa(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return siswaRepository.searchfindAll(search,pageable);
    }

    @Override
    public Siswa getSiswa(Long id) {
        var mapel = siswaRepository.findById(id).get();
        return siswaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Siswa addSiswa(SiswaDto siswa) {
        Siswa person = new Siswa();
        person.setNamaSiswa(siswa.getNamaSiswa());
        person.setTempatLahir(siswa.getTempatLahir());
        person.setTanggalLahir(siswa.getTanggalLahir());
        person.setKelas(siswa.getKelas());
        person.setAlamat(siswa.getAlamat());
        return siswaRepository.save(person);
    }

    @Override
    public Siswa editSiswa(Long id, Siswa siswa) {
        Siswa mata = siswaRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        mata.setNamaSiswa(siswa.getNamaSiswa());
        mata.setTempatLahir(siswa.getTempatLahir());
        mata.setTanggalLahir(siswa.getTanggalLahir());
        mata.setKelas(siswa.getKelas());
        mata.setAlamat(siswa.getAlamat());
        return siswaRepository.save(mata);
    }

    @Override
    public Map<String, Boolean> deleteSiswaById(Long id) {
        try {
            siswaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
