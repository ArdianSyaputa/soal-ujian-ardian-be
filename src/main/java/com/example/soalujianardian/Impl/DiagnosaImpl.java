package com.example.soalujianardian.Impl;

import com.example.soalujianardian.exception.NotFoundException;
import com.example.soalujianardian.dto.DiagnosaDto;
import com.example.soalujianardian.model.Diagnosa;
import com.example.soalujianardian.repository.DiagnosaRepository;
import com.example.soalujianardian.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DiagnosaImpl implements DiagnosaService {

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Override
    public Page<Diagnosa> getAllDiagnosa(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return diagnosaRepository.searchfindAll(search,pageable);
    }

    @Override
    public Diagnosa getDiagnosa(Long id) {
        var diag = diagnosaRepository.findById(id).get();
        return diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Diagnosa addDiagnosa(DiagnosaDto diagnosa) {
        Diagnosa diag = new Diagnosa();
        diag.setNama_Diangnosa(diagnosa.getNama_Diagnosa());
        return diagnosaRepository.save(diag);
    }

    @Override
    public Diagnosa editDiagnosa(Long id, Diagnosa diagnosa) {
        Diagnosa diag = diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        diag.setNama_Diangnosa(diagnosa.getNama_Diangnosa());
        return diagnosaRepository.save(diag);
    }

    @Override
    public Map<String, Boolean> deleteDiagnosaById(Long id) {
        try {
            diagnosaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
