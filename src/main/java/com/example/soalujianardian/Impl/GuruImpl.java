package com.example.soalujianardian.Impl;

import com.example.soalujianardian.exception.NotFoundException;
import com.example.soalujianardian.dto.GuruDto;
import com.example.soalujianardian.model.Guru;
import com.example.soalujianardian.repository.GuruRepository;
import com.example.soalujianardian.service.GuruService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@Service
public class GuruImpl implements GuruService {

    @Autowired
    GuruRepository guruRepository;


    @Override
    public Page<Guru> getAllGuru(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return guruRepository.searchfindAll(search,pageable);
    }

    @Override
    public Guru getGuru(Long id) {
        var mapel = guruRepository.findById(id).get();
        return guruRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Guru addGuru(GuruDto guru) {
        Guru mata = new Guru();
        mata.setNamaGuru(guru.getNamaGuru());
        mata.setTempatLahir(guru.getTempatLahir());
        mata.setTanggalLahir(guru.getTanggalLahir());
        mata.setAlamat(guru.getAlamat());
        return guruRepository.save(mata);
    }

    @Override
    public Guru editGuru(Long id, Guru guru) {
        Guru mata = guruRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        mata.setNamaGuru(guru.getNamaGuru());
        mata.setTempatLahir(guru.getTempatLahir());
        mata.setTanggalLahir(guru.getTanggalLahir());
        mata.setAlamat(guru.getAlamat());
        return null;
    }

    @Override
    public Map<String, Boolean> deleteGuruById(Long id) {
        try {
            guruRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
