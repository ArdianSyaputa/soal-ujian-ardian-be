package com.example.soalujianardian.Impl;
;
import com.example.soalujianardian.dto.PeriksaDto;
import com.example.soalujianardian.exception.NotFoundException;
import com.example.soalujianardian.model.Periksa;
import com.example.soalujianardian.repository.GuruRepository;
import com.example.soalujianardian.repository.KaryawanRepository;
import com.example.soalujianardian.repository.PeriksaRepository;
import com.example.soalujianardian.repository.SiswaRepository;
import com.example.soalujianardian.service.PeriksaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PeriksaImpl implements PeriksaService {

    @Autowired
    PeriksaRepository periksaRepository;

    @Autowired
    SiswaRepository siswaRepository;

    @Autowired
    GuruRepository guruRepository;

    @Autowired
    KaryawanRepository karyawanRepository;


    @Override
    public Page<Periksa> getAllWarga(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return periksaRepository.searchfindAll(search,pageable);
    }

    @Override
    public Periksa getWarga(Long id) {
        var periksa = periksaRepository.findById(id).get();
        return periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Periksa addWarga(PeriksaDto periksa) {
        Periksa per = new Periksa();
        per.setKeterangan(periksa.getKeterangan());
        per.setNamaSiswa(siswaRepository.findById(periksa.getNamaSiswa()).get());
        per.setNamaGuru(guruRepository.findById(periksa.getNamaGuru()).get());
        per.setNamaKaryawan(karyawanRepository.findById(periksa.getNamaKaryawan()).get());
        return periksaRepository.save(per);
    }

    @Override
    public Periksa editWarga(Long id, Periksa periksa) {
        Periksa per = periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        per.setKeterangan(periksa.getKeterangan());
        return periksaRepository.save(per);
    }

    @Override
    public Map<String, Boolean> deleteWargaById(Long id) {
        try {
            periksaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
