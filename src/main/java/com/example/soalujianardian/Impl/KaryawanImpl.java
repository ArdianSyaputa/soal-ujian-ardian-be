package com.example.soalujianardian.Impl;

import com.example.soalujianardian.exception.NotFoundException;
import com.example.soalujianardian.dto.KaryawanDto;
import com.example.soalujianardian.model.Karyawan;
import com.example.soalujianardian.repository.KaryawanRepository;
import com.example.soalujianardian.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class KaryawanImpl implements KaryawanService {

    @Autowired
    KaryawanRepository karyawanRepository;

    @Override
    public Page<Karyawan> getAllWarga(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return karyawanRepository.searchfindAll(search,pageable);
    }

    @Override
    public Karyawan getWarga(Long id) {
        var karyawan = karyawanRepository.findById(id).get();
        return karyawanRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Karyawan addWarga(KaryawanDto karyawan) {
        Karyawan orang = new Karyawan();
        orang.setNamaKaryawan(karyawan.getNamaKaryawan());
        orang.setTempatLahir(karyawan.getTempatLahir());
        orang.setTanggalLahir(karyawan.getTanggalLahir());
        orang.setAlamat(karyawan.getAlamat());
        return karyawanRepository.save(orang);
    }

    @Override
    public Karyawan editWarga(Long id, Karyawan karyawan) {
        Karyawan orang = karyawanRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        orang.setNamaKaryawan(karyawan.getNamaKaryawan());
        orang.setTempatLahir(karyawan.getTempatLahir());
        orang.setTanggalLahir(karyawan.getTanggalLahir());
        orang.setAlamat(karyawan.getAlamat());
        return karyawanRepository.save(orang);
    }

    @Override
    public Map<String, Boolean> deleteWargaById(Long id) {
        try {
            karyawanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
