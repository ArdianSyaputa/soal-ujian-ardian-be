package com.example.soalujianardian.Impl;

import com.example.soalujianardian.model.DaftarObat;
import com.example.soalujianardian.repository.DaftarObatRepository;
import com.example.soalujianardian.service.DaftarObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DaftarObatImpl implements DaftarObatService {

    @Autowired
    DaftarObatRepository daftarObatRepository;

    @Override
    public Page<DaftarObat> getAllDaftarObat(String search, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return daftarObatRepository.searchfindAll(search,pageable);
    }

    @Override
    public DaftarObat getDaftarObat(Long id) {
        return null;
    }

    @Override
    public DaftarObat addDaftarObat(DaftarObat daftarObat) {
        return null;
    }

    @Override
    public DaftarObat editDaftarObat(Long id, DaftarObat daftarObat) {
        return null;
    }

    @Override
    public Map<String, Boolean> deleteDaftarObatById(Long id) {
        return null;
    }
}
