package com.example.soalujianardian.Login.Repository;


import com.example.soalujianardian.Login.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<TemporaryToken, Long> {
    Optional<TemporaryToken> findByToken(String token);
    Optional<TemporaryToken> findBySekolahId(long sekolahId);

}
