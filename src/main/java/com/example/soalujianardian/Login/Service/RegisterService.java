package com.example.soalujianardian.Login.Service;



import com.example.soalujianardian.Login.model.Login;
import com.example.soalujianardian.Login.model.Register;
import com.google.api.gax.paging.Page;

import java.util.Map;

public interface RegisterService {
    Register registrasi(Register sekolah);

    Map<String, Object> login(Login login);

    Register getById(Long id);

    Register update(Long id, Register sekolah);

    Map<String, Boolean> deleteSekolah(Long id);

    Page<Register> getAll(String query, Long page);
}
