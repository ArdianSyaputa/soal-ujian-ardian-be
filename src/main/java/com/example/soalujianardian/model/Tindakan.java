package com.example.soalujianardian.model;

import javax.persistence.*;

@Entity
@Table(name = "tindakan")
public class Tindakan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "nama_Tindakan")
    private String nama_Tindakan;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama_Tindakan() {
        return nama_Tindakan;
    }

    public void setNama_Tindakan(String nama_Tindakan) {
        this.nama_Tindakan = nama_Tindakan;
    }
}
