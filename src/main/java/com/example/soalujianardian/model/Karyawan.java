package com.example.soalujianardian.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.lang.String;

@Entity
@Table(name = "karyawan")
public class Karyawan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "nama_karyawan")
    private String namaKaryawan;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "alamat")
    private String alamat;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tanggal_lahir")
    private String tanggalLahir;


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}
