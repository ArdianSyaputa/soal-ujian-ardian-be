package com.example.soalujianardian.model;

import javax.persistence.*;

@Entity
@Table(name = "periksa")
public class Periksa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "nama_siswa")
    private Siswa namaSiswa;
    @ManyToOne
    @JoinColumn(name = "nama_guru")
    private Guru namaGuru;
    @ManyToOne
    @JoinColumn(name = "nama_karyawan")
    private Karyawan namaKaryawan;

    @Column(name = "keterangan")
    private String keterangan;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Siswa getNamaSiswa() {
        return namaSiswa;
    }

    public void setNamaSiswa(Siswa namaSiswa) {
        this.namaSiswa = namaSiswa;
    }

    public Guru getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(Guru namaGuru) {
        this.namaGuru = namaGuru;
    }

    public Karyawan getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(Karyawan namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
