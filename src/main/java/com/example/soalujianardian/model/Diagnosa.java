package com.example.soalujianardian.model;

import javax.persistence.*;

@Entity
@Table(name = "diagnosa")
public class Diagnosa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "nama_diagnosa")
    private String nama_Diangnosa;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama_Diangnosa() {
        return nama_Diangnosa;
    }

    public void setNama_Diangnosa(String nama_Diangnosa) {
        this.nama_Diangnosa = nama_Diangnosa;
    }
}
